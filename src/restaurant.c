/*
  iamhisoka
*/

#include "../include/shm.h"

/* char *execute() */
/* { */
/*      int p = 0, a = 0; */
/*      char *tab = NULL; */
/*     size_t z = 0; */
/*     ssize_t s = 0; */
/*      p = fork(); */
/*      (p == 0) ? a = execve("./chrono > .lol", NULL, NULL) : a; */
/*     FILE *fd = fopen(".lol", "r"); */
/*     s = getline(&tab, &z, fd); */
/*     (s > 0) ? tab[s - 1] = '\0' : tab[s - 1]; */
/*      return (tab); */
/* } */

/* int start_chrono(int y) */
/* { */
/*     char **tab = NULL; */
/*     size_t a = 0; */
/*     ssize_t s = 0; */
/*     int i = 0; */
/*     FILE *fd = fopen(".time", "r"); */
/*     int f = 0; */
/*     char *time = malloc(sizeof(char) * 50); */
/*     tab = malloc(sizeof(char *) * 50); */
/*     for (i = 0; s != -1; i++) { */
/*         tab[i] = NULL; */
/*         a = 0; */
/*         s = getline(&tab[i], &a, fd); */
/*         (s > 0) ? tab[i][s - 1] = '\0' : tab[i][s - 1]; */
/*     } */
/*     tab[i - 1] = NULL; */
/*     for (f = 0; tab[f][0] != y + 48; f++); */
/*     tab[f][2] = '\0'; */
/*     time = execute(); */
/*     tab[f] = strcat(tab[f], time); */
/*     return (0); */
/* } */

int manage()
{
    char **tab = NULL;
    size_t a = 0;
    ssize_t s = 0;
    int i = 0;
    FILE *fd = fopen(".resto", "r");

    tab = malloc(sizeof(char *) * 50);
    for (i = 0; s != -1; i++) {
        tab[i] = NULL;
        a = 0;
        s = getline(&tab[i], &a, fd);
        (s > 0) ? tab[i][s - 1] = '\0' : tab[i][s - 1];
    }
    tab[i - 1] = NULL;

    /* for (; all_finish == 0 ; ) */
    /* 	for (int q = 0, w = 0; tab[w] != NULL; w++) { */
    /* 	    q = tab[w][strlen(tab[w]) - 3] - 48; */
    /* 	    if (q == 0) { */
    /* 		    tab[w][strlen(tab[w]) - 3] = '2'; */
    /* 		    start_chrono(tab[w][0] - 48); */
    /* 	    } */
    /* } */
    return (0);
}

int gerance(char *av[])
{
    char str[10] = "Table 0 : ", ok[2] = "0#";
    int fd = open(".resto", O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR),
    ld = open(".time", O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR),
    sd = open(".annuaire", O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
    write(fd, av[1], strlen(av[1]));
    write(fd, "\n", 1);
    for (int d = 7; d > 0; d--) {
	for (int i = 2; av[i] != NULL; i++) {
	    if (d == av[i][0] - 48) {
	       ok[0] = i - 2 + 48; 
	       write(fd, ok, 2);
	       write(fd, av[i], strlen(av[i]));
	       write(fd, "#EMPTY#EMPTY#0#0#0\n", 19);
	       write(ld, ok, 2);
	       write(ld, "0\n", 2);
	       str[6] = i - 2 + 48;
	       write(sd, str, 10);
	       write(sd, "(vide)\n", 7);
	    }
	}
    }
    write(sd, "cahier de rappels :\n", 20);
    close(fd);
    close(ld);
    close(sd);
/////////////    manage();
    return (0);
}

int is_good(int ac, char *av[])
{
    if (ac < 3) {
       printf("usage: ./restaurant duréedurepas table0 table1 ...");
       return (1);
    }
    for (int j = 1; j < ac; j++) {
        for (int i = 0; av[j][i] != '\0'; i++)
            if (av[j][i] < '0' || av[j][i] > '9') {
                printf("usage: ./restaurant duréedurepas table0 table1 ...");
                return (1);
            }
        if ((atoi(av[j]) == 0) || (j > 1 && atoi(av[j]) > 6)) {
            printf("usage: ./restaurant duréedurepas table0 table1 ...");
            return (1);
        }
    }
    return (0);
}

int main(int ac, char *av[])
{
    if (is_good(ac, av) == 1)
        return (1);
    gerance(av);
    return (0);
}
