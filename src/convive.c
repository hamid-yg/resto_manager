#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../include/shm.h"

int main(int argc, char **argv)
{
    int check = check_error_convive(argc, argv);
    struct stat info;
    t_control *control = NULL;

    if (check || stat(".resto", &info) != 0)
        return (84);
    control = create_control();
    manage_convive(control, argv);
    write_resto(control);
    free_control(control);
    return (0);
}

int is_letter(char c)
{
    if (c >= 'a' && c <= 'z')
        return (1);
    if (c >= 'A' && c <= 'Z')
        return (1);
    return (0);
}

void manage_convive(t_control *control, char **argv)
{
    t_place *place = NULL;
    t_occupant *occupant = NULL;

    if (!is_letter(argv[2][0])) {
        place = get_place_nbr(control, argv[2][0] - '0');
        if (!place)
            printf("Désolé %s, pas de table disponible\n", argv[1]);
        else {
            place->leader = strdup(argv[1]);
            place->wish = argv[2][0] - '0';
            printf("Bienvenue %s, vous avez la table %d", argv[1], place->index);
        }
    } else {
        place = get_place_lead(control, argv[2]);
        if (!place)
            printf("Désolé %s, pas de %s ici\n", argv[2], argv[1]);
        else if (place->wish - place->place == 0)
            printf("Désolé %s, plus de place disponible à la table %d\n", argv[1], place->index);
        else {
            occupant = create_occupant(argv[1]);
            add_occupant_list(place, occupant);
            printf("Bienvenue %s, vous avez la table %d", argv[1], place->index);
        }
    }
}

t_place *get_place_lead(t_control *control, char *str)
{
    t_place *place = control->head;

    while (place) {
        if (place->leader) {
            if (!strcmp(place->leader, str))
                return (place);
        }
        place = place->next;
    }
    return (NULL);
}

t_place *get_place_nbr(t_control *control, int size)
{
    t_place *place = control->head;

    while (place) {
        if (!place->leader && place->place_max - size >= 0)
            return (place);
        place = place->next;
    }
    return (NULL);
}

int check_error_convive(int ac, char **av)
{
    if (ac != 3) {
        printf("usage: ./convive ...");
        return (1);
    }
    if ((!is_letter(av[1][0])) || strlen(av[1]) > 10) {
        printf("usage: ./convive ...");
        return (1);
    }
    if ((av[2][0] > '0' && av[2][0] < '8') && strlen(av[2]) == 1)
        return (0);
    else if ((!is_letter(av[2][0])) || strlen(av[2]) > 10) {
        printf("usage: ./convive ...");
        return (1);
    }
    return (0);
}

void write_resto(t_control *control)
{
    int fd = open(".resto", O_WRONLY | O_TRUNC);
    t_place *place = control->head;
    t_occupant *occupant = NULL;
    char c = 0;

    write(fd, control->time, strlen(control->time));
    write(fd, "\n", 1);
    while (place) {
        write(fd, place->index_str, strlen(place->index_str));
        write(fd, "#", 1);
        write(fd, place->place_str, strlen(place->place_str));
        write(fd, "#", 1);
        if (place->leader)
            write(fd, place->leader, strlen(place->leader));
        else
            write(fd, "EMPTY", 5);
        write(fd, "#", 1);
        if (!place->head_occupant)
            write(fd, "EMPTY", 5);
        else {
            occupant = place->head_occupant;
            while (occupant) {
                write(fd, occupant->str, strlen(occupant->str));
                occupant = occupant->next;
                if (occupant)
                    write(fd, ",", 1);
            }
        }
        write(fd, "#", 1);
        c = place->wish + '0';
        write(fd, &c, 1);
        write(fd, "#", 1);
        c = place->is_full + '0';
        write(fd, &c, 1);
        write(fd, "\n", 1);
        place = place->next;
    }
    close(fd);
}

t_control *create_control(void)
{
    int fd = open(".resto", O_RDONLY);
    t_control *control = (t_control *)malloc(sizeof(t_control));
    char *file = NULL;
    struct stat info;
    int size = 0;
    char **line = NULL;
    t_place *place = NULL;

    control->head = NULL;
    control->tail = NULL;
    stat(".resto", &info);
    file = (char *)malloc(sizeof(char) * (info.st_size + 1));
    read(fd, file, info.st_size);
    file[info.st_size] = '\0';
    size = count_char(file, '\n') + 1;
    line = (char **)malloc(sizeof(char *) * (size + 1));
    control->time = strdup(strtok(file, "\n"));
    for (int i = 0; i < size; i++)
        line[i] = strtok(NULL, "\n");
    line[size] = NULL;
    for (int i = 0; line[i]; i++) {
        place = create_place(line[i]);
        add_place_list(control, place);
    }
    free_array((void **)line);
    free(file);
    close(fd);
    return (control);
}

t_place *create_place(char *line)
{
    t_occupant *occupant = NULL;
    t_place *place = (t_place *)malloc(sizeof(t_place));
    char *str = NULL;
    char *occ = NULL;

    place->next = NULL;
    place->prev = NULL;
    place->index_str = strdup(strtok(line, "#"));
    place->index = atoi(place->index_str);
    place->place_str = strdup(strtok(NULL, "#"));
    place->place_max = atoi(place->place_str);
    place->tail_occupant = NULL;
    place->head_occupant = NULL;
    str = strtok(NULL, "#");
    str = ((!strcmp("EMPTY", str)) ? NULL : strdup(str));
    place->leader = str;
    occ = strdup(strtok(NULL, "#"));
    str = strtok(NULL, "#");
    place->wish = str[0] - '0';
    str = strtok(NULL, "#");
    place->is_full = str[0] - '0';
    fill_occupant(place, occ);
    place->place = ((!place->leader) ? 0 : 1);
    occupant = place->head_occupant;
    while (occupant) {
        (place->place)++;
        occupant = occupant->next;
    }
    free(occ);
    return (place);
}

void print_place(t_place *place)
{
    t_occupant *occupant = NULL;

    printf("place: index %d, place %d lead [%s] ", place->index, place->place_max, place->leader);
    if (!place->head_occupant) {
        printf("\n");
        return;
    }
    printf("ocupant ");
    occupant = place->head_occupant;
    while (occupant) {
        printf("%s ", occupant->str);
        occupant = occupant->next;
    }
    printf("\n");
}

void fill_occupant(t_place *place, char *str)
{
    char *str_temp = NULL;
    t_occupant *occupant = NULL;

    if (!str)
        return;
    if (!strcmp(str, "EMPTY"))
        return;
    str_temp = strtok(str, ",");
    if (str_temp) {
        occupant = create_occupant(str_temp);
        add_occupant_list(place, occupant);
    }
    str_temp = strtok(NULL, ",");
    while (str_temp){
        occupant = create_occupant(str_temp);
        add_occupant_list(place, occupant);
        str_temp = strtok(NULL, ",");
    }
}

void add_occupant_list(t_place *place, t_occupant *element)
{
    if (!place->head_occupant)
        place->head_occupant = element;
    else {
        if (!place->tail_occupant) {
            place->head_occupant->next = element;
            element->prev = place->head_occupant;
        } else {
            place->tail_occupant->next = element;
            element->prev = place->tail_occupant;
        }
        place->tail_occupant = element;
    }
}

t_occupant *create_occupant(char *str)
{
    t_occupant *occupant = (t_occupant *)malloc(sizeof(t_occupant));

    occupant->next = NULL;
    occupant->prev = NULL;
    occupant->str = strdup(str);
    return (occupant);
}

void add_place_list(t_control *control, t_place *place)
{
    if (!control->head)
        control->head = place;
    else {
        if (!control->tail) {
            control->head->next = place;
            place->prev = control->head;
        } else {
            control->tail->next = place;
            place->prev = control->tail;
        }
        control->tail = place;
    }
}

int count_char(char *str, char c)
{
    int size = 0;
    for (int i = 0; str[i]; i++)
        ((str[i] == c) ? size++ : size);
    return (size);
}

void free_occupant_list(t_occupant *element)
{
    t_occupant *element_next = ((element) ? element->next : NULL);

    while (element) {
        free(element->str);
        free(element);
        element = element_next;
        element_next = ((element) ? element->next : NULL);
    }
}

void free_control(t_control *control)
{
    t_place *place = ((control) ? control->head : NULL);
    t_place *place_next = ((place) ? place->next : NULL);

    while (place) {
        if (place->leader)
            free(place->leader);
        free(place->index_str);
        free(place->place_str);
        if (place->head_occupant)
            free_occupant_list(place->head_occupant);
        free(place);
        place = place_next;
        place_next = ((place) ? place->next : NULL);
    }
    free(control->time);
    free(control);
}

void free_array(void **arr)
{
    if (!arr)
        return;
    free(arr);
}
