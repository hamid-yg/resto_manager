#ifndef SHM_H
#define SHM_H

// Tous les headers ici
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// Tous les prototypes ici
// restaurant
int gerance(char *av[]);
int is_good(int ac, char *av[]);

// convive

// police

// fermetture
typedef struct t_occupant {
    char *str;
    struct t_occupant *next;
    struct t_occupant *prev;
}t_occupant;

typedef struct t_place {
    int index;
    int place_max;
    int place;
    int wish;
    int is_full;
    char *index_str;
    char *place_str;
    char *leader;
    t_occupant *head_occupant;
    t_occupant *tail_occupant;
    struct t_place *next;
    struct t_place *prev;
}t_place;

typedef struct t_control {
    char *time;
    t_place *head;
    t_place *tail;
}t_control;

t_place *get_place_lead(t_control *control, char *str);
t_place *get_place_nbr(t_control *control, int size);
void manage_convive(t_control *control, char **argv);
int is_letter(char c);
int check_error_convive(int argc, char **argv);
t_occupant *create_occupant(char *str);
void add_occupant_list(t_place *place, t_occupant *occupant);
void free_occupant_list(t_occupant *occupant);
void write_resto(t_control *control);
void print_place(t_place *place);
void fill_occupant(t_place *place, char *str);
t_place *create_place(char *line);
void add_place_list(t_control *control, t_place *place);
void free_control(t_control *control);
t_control *create_control(void);
int count_char(char *str, char c);
void free_array(void **arr);
#endif
